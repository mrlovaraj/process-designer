package org.comsoft.jbpm.designer.web.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jbpm.designer.web.profile.IDiagramProfile;
import org.jbpm.designer.web.profile.impl.ExternalInfo;
import org.jbpm.designer.web.server.ServletUtil;

/**
 * Provide process definition extensions in JSON format
 * 
 * @author <a href="mailto:faa@comsoft-corp.ru">Fomichev Artem</a> <br>
 *
 */
public class ExtensionsEditorServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger _logger = Logger.getLogger(ExtensionsEditorServlet.class);

	private static final String ACTION_LOAD = "load";
	private static final String ACTION_GET_DATATYPES = "getDataTypes";
	private static final String ACTION_GET_COMPONENTS = "getComponents";

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		String uuid = req.getParameter("uuid");
		String profileName = req.getParameter("profile");

		IDiagramProfile profile = profileName != null ? ServletUtil.getProfile(req, profileName, getServletContext()) : null;

		if(action != null) {
			if (action.equals(ACTION_LOAD)) {
				resp.setContentType("text/html");
				resp.setCharacterEncoding("UTF-8");
				PrintWriter pw = resp.getWriter();
				pw.write(getExtensionFromRepo(uuid, profile));
			} else if (action.equals(ACTION_GET_DATATYPES)) {
				String dataTypesJsonString;
				try {
					dataTypesJsonString = getDataTypesFromOpenIcar(profile);
				} catch (IOException e) {
					e.printStackTrace();
					resp.setStatus(404);
					return;
				}
				resp.setContentType("application/json");
				resp.setCharacterEncoding("UTF-8");
				PrintWriter pw = resp.getWriter();
				pw.write(dataTypesJsonString);
			} else if (action.equals(ACTION_GET_COMPONENTS)) {
				String componentsJsonString;
				try {
					componentsJsonString = getComponentsFromOpenIcar(profile);
				} catch (IOException e) {
					e.printStackTrace();
					resp.setStatus(404);
					return;
				}
				resp.setContentType("application/json");
				resp.setCharacterEncoding("UTF-8");
				PrintWriter pw = resp.getWriter();
				pw.write(componentsJsonString);
			}
		}
	}

	private String getDataTypesFromOpenIcar(IDiagramProfile profile) throws IOException {
		String dataTypesURL = ExternalInfo.getExternalProtocol(profile)
				+ "://"
				+ ExternalInfo.getExternalHost(profile)
				+ "/"
				+ profile.getExternalLoadURLSubdomain().substring(0,
						profile.getExternalLoadURLSubdomain().indexOf("/"))
				+ "/rest/datatypesjson/";

		URL url = new URL(dataTypesURL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		ServletUtil.applyAuth(profile, connection);
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept","application/json,application/octet-stream,text/json,text/plain;q=0.9,*/*;q=0.8");
		connection.setRequestProperty("charset", "UTF-8");
		connection.setRequestProperty("Accept-Charset", "UTF-8");
		connection.setReadTimeout(5 * 1000);
		connection.setConnectTimeout(5000);
		connection.connect();
		_logger.info("check connection response code: " + connection.getResponseCode());
		if (connection.getResponseCode() == 200) {
			return connectionStreamToString(connection);
		}
		return null;
	}

	private String getComponentsFromOpenIcar(IDiagramProfile profile) throws IOException {
		String dataTypesURL = ExternalInfo.getExternalProtocol(profile)
				+ "://"
				+ ExternalInfo.getExternalHost(profile)
				+ "/"
				+ profile.getExternalLoadURLSubdomain().substring(0,
						profile.getExternalLoadURLSubdomain().indexOf("/"))
				+ "/rest/componentsjson/";

		URL url = new URL(dataTypesURL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		ServletUtil.applyAuth(profile, connection);
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept","application/json,application/octet-stream,text/json,text/plain;q=0.9,*/*;q=0.8");
		connection.setRequestProperty("charset", "UTF-8");
		connection.setRequestProperty("Accept-Charset", "UTF-8");
		connection.setReadTimeout(5 * 1000);
		connection.setConnectTimeout(5000);
		connection.connect();
		_logger.info("check connection response code: " + connection.getResponseCode());
		if (connection.getResponseCode() == 200) {
			return connectionStreamToString(connection);
		}
		return null;
	}

	protected String connectionStreamToString(HttpURLConnection connection) throws UnsupportedEncodingException, IOException {
		BufferedReader sreader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
		StringBuilder stringBuilder = new StringBuilder();
		String line = null;
		while ((line = sreader.readLine()) != null) {
			stringBuilder.append(line + "\n");
		}
		return stringBuilder.toString();
	}

	private String getExtensionFromRepo(String uuid, IDiagramProfile profile) {
		try {
			String taskFormSourceURL = ExternalInfo.getExternalProtocol(profile)
					+ "://"
					+ ExternalInfo.getExternalHost(profile)
					+ "/"
					+ profile.getExternalLoadURLSubdomain().substring(0,
							profile.getExternalLoadURLSubdomain().indexOf("/"))
					+ "/rest/extensionjson/"+ URLEncoder.encode(uuid, "UTF-8");

			URL url = new URL(taskFormSourceURL);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			ServletUtil.applyAuth(profile, connection);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept","application/json,application/octet-stream,text/json,text/plain;q=0.9,*/*;q=0.8");
			connection.setRequestProperty("charset", "UTF-8");
			connection.setRequestProperty("Accept-Charset", "UTF-8");
			connection.setReadTimeout(5 * 1000);
			connection.setConnectTimeout(5000);
			connection.connect();
			_logger.info("check connection response code: " + connection.getResponseCode());
			if (connection.getResponseCode() == 200) {
				return connectionStreamToString(connection);
			}
		} catch (Exception e) {
			_logger.error(e.getMessage());
		}
		return "false";
	}
}
