if (!ORYX.Plugins) 
	ORYX.Plugins = {};

if (!ORYX.Config)
	ORYX.Config = {};

if(!COMSOFT) var COMSOFT = {};

COMSOFT.Extensions = Clazz.extend({
	model: undefined,
	dataTypes: undefined,
	components: undefined,
	deletedTasks: new Array(),
	deletedSequenceFlows: new Array(),
	/*
	construct: function(config) {
		
	},
	 */
	getExtensionsJson: function() {
		if (this.model) {
			if (this.deletedTasks.length == 0 && this.deletedSequenceFlows.length == 0) {
				return Ext.util.JSON.encode(this.model);
			} else {
				var model = this.model;
				var modelClone = new Hash();
				var deletedTasks = this.deletedTasks;
				var deletedSequenceFlows = this.deletedSequenceFlows;
				for (modelKey in model) {
					if (model.hasOwnProperty(modelKey)) {
						var modelValue = model[modelKey];
						if (modelValue.hasOwnProperty('length') && modelValue.push) {
							var arrayClone = new Array();
							for (var i = 0; i < modelValue.length; i++) {
								var el = modelValue[i];
								if (el.hasOwnProperty('task') && deletedTasks.indexOf(el.task) >= 0) {
									// skip element
								} else if (el.hasOwnProperty('subjectId') && el.hasOwnProperty('subjectType')) {
									if (el.subjectType === 'HumanTask' && deletedTasks.indexOf(el.subjectId) >= 0) {
										// skip element
									} else if (el.subjectType === 'SequenceFlow') {
										if (deletedSequenceFlows.indexOf(el.subjectId) >= 0) {
											//skip element
										} else if (deletedTasks.indexOf(el.subjectId.substring(0, el.subjectId.indexOf(';'))) >= 0) {
											//skip element
										} else {
											arrayClone.push(el);
										}
									} else {
										arrayClone.push(el);
									}
								} else {
									arrayClone.push(el);
								}
							}
							modelClone[modelKey] = arrayClone;
						} else {
							modelClone[modelKey] = modelValue;
						}
					}
				}
				return Ext.util.JSON.encode(modelClone);
			}
		} else {
			return '';
		}
	},
	loadModel: function() {
        var processJSON = ORYX.EDITOR.getSerializedJSON();
        var processId = jsonPath(processJSON.evalJSON(), "$.properties.id");
		Ext.Ajax.request({
			url: ORYX.PATH + 'extensionseditor',
			method: 'POST',
			success: function(response) {
				try {
					var responseText = response.responseText;
					COMSOFT.EXTENSIONS.model = Ext.util.JSON.decode(responseText);
					if (!COMSOFT.EXTENSIONS.model) {
						COMSOFT.EXTENSIONS.model = new Hash();
						COMSOFT.EXTENSIONS.model.processKey = processId + '';
					}
				} catch(e) {
					Ext.Msg.minWidth = 360;
					Ext.Msg.alert('Error initiating Extensions:\n' + e);
				}
			},
			failure: function(){
				Ext.Msg.minWidth = 360;
				Ext.Msg.alert('Error initiating Extensions.');
			},
			params: {
				action: 'load',
				//taskname: options.tn,
				profile: ORYX.PROFILE,
				uuid : ORYX.UUID
			}
		});
	},
	loadDataTypes: function() {
		Ext.Ajax.request({
			url: ORYX.PATH + 'extensionseditor',
			method: 'POST',
			success: function(response) {
				try {
					var responseText = response.responseText;
					COMSOFT.EXTENSIONS.dataTypes = Ext.util.JSON.decode(responseText);
				} catch(e) {
					Ext.Msg.minWidth = 360;
					Ext.Msg.alert('Error initiating openicar data types :\n' + e);
				}
			},
			failure: function(e){
				console.warn('openicar data types not available... using standard types');
			},
			params: {
				action: 'getDataTypes',
				profile: ORYX.PROFILE
			}
		});
	},
	loadComponents: function() {
		Ext.Ajax.request({
			url: ORYX.PATH + 'extensionseditor',
			method: 'POST',
			success: function(response) {
				try {
					var responseText = response.responseText;
					var components = Ext.util.JSON.decode(responseText);
					var componentMethodsMap = new Hash();
					for (var i = 0; i < components.length; i++)
						componentMethodsMap[components[i].name] = components[i].methods;
					COMSOFT.EXTENSIONS.components = componentMethodsMap;
				} catch(e) {
					Ext.Msg.minWidth = 360;
					Ext.Msg.alert('Error initiating openicar components :\n' + e);
				}
			},
			failure: function(e){
				console.warn('openicar components not available...');
			},
			params: {
				action: 'getComponents',
				profile: ORYX.PROFILE
			}
		});		
	}
});

if (!COMSOFT.EXTENSIONS) COMSOFT.EXTENSIONS = new COMSOFT.Extensions();

Ext.form.ComboBoxEx = Ext.extend(Ext.form.ComboBox, {
	allowArbitraryText: false,
	lastFilterValue: '',
	doQuery : function(C, B) {
		if (C === undefined || C === null) {
			C = ""
		}
		var A = {
			query : C,
			forceAll : B,
			combo : this,
			cancel : false
		};
		if (this.fireEvent("beforequery", A) === false
				|| A.cancel) {
			return false
		}
		C = A.query;
		B = A.forceAll;
		if (B === true || (C.length >= this.minChars)) {
			if (this.lastQuery !== C) {
				this.lastQuery = C;
				if (this.mode == "local") {
					this.selectedIndex = -1;
					if (B) {
						this.store.clearFilter()
					} else {
						this.store.filter(this.displayField, C)
					}
					this.lastFilterValue = C;
					this.onLoad()
				} else {
					this.store.baseParams[this.queryParam] = C;
					this.store.load({
						params : this.getParams(C)
					});
					this.expand()
				}
			} else {
				this.selectedIndex = -1;
				this.onLoad()
			}
		}
	},
	onLoad : function() {
		if (!this.hasFocus) {
			return
		}
		/*
		if (this.store.getCount() == 0 && this.allowNewOptions && this.lastFilterValue && this.lastFilterValue.length > 0) {
			var fieldValues = new Hash();
			fieldValues[this.displayField] = this.lastFilterValue;
			fieldValues[this.displayField] = this.lastFilterValue;
			var newRec = new Ext.data.Record(fieldValues);
			this.store.add(newRec);			
			this.store.filter(this.displayField, C)
		}
		 */
		if (this.store.getCount() > 0) {
			this.expand();
			this.restrictHeight();
			if (this.lastQuery == this.allQuery) {
				if (this.editable) {
					this.el.dom.select()
				}
				if (!this.selectByValue(this.value, true)) {
					this.select(0, true)
				}
			} else {
				this.selectNext();
				if (this.typeAhead
						&& this.lastKey != Ext.EventObject.BACKSPACE
						&& this.lastKey != Ext.EventObject.DELETE) {
					this.taTask.delay(this.typeAheadDelay)
				}
			}
		} else {
			if (this.allowArbitraryText && this.lastFilterValue.length > 0) {
				this.value = this.lastFilterValue;
				this.setRawValue(this.lastFilterValue); 
			}
			this.onEmptyResults();
		}
	}
});

ORYX.Plugins.ExtensionEditor = Clazz.extend({
	//sourceMode: undefined,
	//extensioneditor: undefined,
	//extensionsourceeditor: undefined,
	//extensioncolorsourceeditor: undefined,
	dialog: undefined,
	//hlLine: undefined,

	extensionVariablesDS: undefined,
	extensionTaskVariablesDS: undefined,
	extensionTaskValidationsDS: undefined,
	extensionAttributesDS: undefined,
	extensionSeqFlowAttributesDS: undefined,
	loadedAttributes: undefined,

	cleanup: function() {
		this.extensionVariablesDS = undefined;
		this.extensionTaskVariablesDS = undefined;
		this.extensionTaskValidationsDS = undefined;
		this.extensionAttributesDS = undefined;
		this.extensionSeqFlowAttributesDS = undefined;
		this.loadedAttributes = undefined;
	},

	construct: function(facade){
		this.facade = facade;
		this.facade.registerOnEvent(ORYX.CONFIG.EVENT_LOADED, this.onLoaded.bind(this));
		this.facade.registerOnEvent(ORYX.CONFIG.EVENT_TASKFORM_EDIT, this.showExtensionEditor.bind(this));
		this.facade.registerOnEvent(ORYX.CONFIG.EVENT_PROPERTY_CHANGED, this.propertyChanged.bind(this) );
		this.facade.registerOnEvent(ORYX.CONFIG.EVENT_SHAPEADDED, this.shapeAdded.bind(this));
		this.facade.registerOnEvent(ORYX.CONFIG.EVENT_BEFORE_SHAPE_DELETED, this.beforeShapeDeleted.bind(this));
	},

	onLoaded: function() {
		COMSOFT.EXTENSIONS.loadModel();
		COMSOFT.EXTENSIONS.loadDataTypes();
		COMSOFT.EXTENSIONS.loadComponents();
	},

	showExtensionEditor: function(options) {
		if(options && options.tn) {
			this._buildandshow(options.tn, options.nodename, null);
		} else {
			Ext.Msg.minWidth = 360;
			Ext.Msg.alert('Task Name not specified.');
		}
	},
	_buildandshow: function(tn, nodename, defaultsrc) {
		/*
		if (!this.extensionsourceeditor) {
			var formvalue = "";
			if(defaultsrc && defaultsrc != "false") {
				formvalue = defaultsrc;
			}
			var sourceeditorid = Ext.id();
			this.extensionsourceeditor = new Ext.form.TextArea({
				id: sourceeditorid,
				//anchor: '100%',
				readOnly: true,
				autoScroll: true,
				value: formvalue
			});
		}
		 */

		this.cleanup();

		var isProcess = !nodename;
		var isUserTask = nodename;

		var tabPanelItems = new Array();
		if (isProcess) {
			tabPanelItems.push({
	        	xtype: 'panel',
	        	title: 'Variables (common definition)',
	        	layout: 'fit',
	        	items: this.createVariablesGrid()
	        });
		} else if (isUserTask) {
			tabPanelItems.push({
	        	xtype: 'panel',
	        	title: 'Task variables',
	        	layout: 'fit',
	        	items: this.createTaskVariablesGrid(nodename)
	        });
			tabPanelItems.push({
	        	xtype: 'panel',
	        	title: 'Task validation',
	        	layout: 'fit',
	        	items: this.createTaskValidationsGrid(nodename)
	        });
		}
		var attrSubjectType = (isProcess ? 'Process' : (isUserTask ? 'HumanTask' : undefined));
		var attrSubjectId = (isUserTask ? nodename : undefined);
		tabPanelItems.push({
			xtype: 'panel',
			title: 'Attributes',
			layout: 'fit',
			items: this.createAttributesGrid(attrSubjectType, attrSubjectId)
		});
		if (isUserTask) {
			var outcomes = this.getUserTaskOutcomes(nodename);
			if (outcomes.length > 0)
				tabPanelItems.push({
					xtype: 'panel',
					title: 'SequenceFlow attributes',
					layout: 'fit',
					items: this.createAttributesGrid('SequenceFlow', undefined, this.loadedAttributes, {taskNodeName: nodename, outcomes: outcomes})
				});
		}

		var tabPanel = new Ext.TabPanel({
			header: false,
			//anchor: '100%',
			//layout:'fit',
			autoScroll:true,
			//border : false,
			//autoDestroy:false,
			activeItem: 0,
			items: tabPanelItems
		/*
	    	,
	        listeners: {
	        	'tabchange': function(tp, p) {
	        		tabPanel.doLayout();
	        }}
		 */

		});

		this.dialog = new Ext.Window({
			id          : 'maineditorwindow',
			layout		: 'fit',
			autoCreate	: true, 
			title		: 'Extensions editor: ' + tn , 
			height		: 570, 
			width		: 930, 
			modal		: true,
			collapsible	: false,
			fixedcenter	: true, 
			shadow		: true, 
			resizable   : true,
			proxyDrag	: true,
			autoScroll  : true,
			keys:[{
				key	: 27,
				fn	: function(){
					this.dialog.hide();
				}.bind(this)
			}],
			items		:[tabPanel],
			listeners	:{
				hide: function(){
					this.dialog.destroy();
				}.bind(this)				
			},
			buttons		: [{
				text: ORYX.I18N.PropertyWindow.ok,
				handler: function(){
					var saveLoadMask = new Ext.LoadMask(Ext.getBody(), {msg:'Apply changes'/*'Сохранение *.ext.xml'*/});
					saveLoadMask.show();

					if (isProcess) {
						this.saveVariables();
					} else if (isUserTask) {
						this.saveTaskVariables(nodename);
						this.saveTaskValidations(nodename);
					}
					this.saveAttributes();

					saveLoadMask.hide();
					this.dialog.hide();

				}.bind(this)
			},
			{
				text: ORYX.I18N.PropertyWindow.cancel,
				handler: function(){
					this.dialog.hide()
				}.bind(this)
			}]
		});		
		this.dialog.show();
	},

	createVariablesGrid: function() {
		var processJSON = ORYX.EDITOR.getSerializedJSON();
		var processVars = jsonPath(JSON.parse(processJSON), "$.properties.vardefs");
		var processVarNames = new Array();
		var varData = new Array();
		var dataTypeMap = new Hash();
		var standardTypes = {
				"String" : "java.lang.String",
				"Integer" : "java.lang.Integer",
				"Boolean" : "java.lang.Boolean",
				"Float" : "java.lang.Float",
				"Object" : "java.lang.Object"}

		varData.push(new Array("", "** Variable Definitions **"));
		if(processVars) {
			processVars.forEach(function(item){
				if(item.length > 0) {
					var valueParts = item.split(",");
					for(var i=0; i < valueParts.length; i++) {
						var innerVal = new Array();
						var nextPart = valueParts[i];
						var value = null;
						if(nextPart.indexOf(":") > 0) {
							var innerParts = nextPart.split(":");
							value = innerParts[0];
							type = innerParts[1];
							if (standardTypes[type] != null) type = standardTypes[type];
							dataTypeMap[value] = type;
						} else {
							value = nextPart;
							dataTypeMap[value] = "java.lang.String";
						}
						innerVal.push(value);
						innerVal.push(value);

						varData.push(innerVal);
						
						processVarNames.push(value);
					}
				}
			});
		}

		var availableDataTypes = new Array(new Array("", "** Data Types **"));
		var uniqueTypes = undefined;
		if (!COMSOFT.EXTENSIONS.dataTypes) {
		//if (dataTypeMap) {
			uniqueTypes = jQuery.unique(dataTypeMap.values());
			uniqueTypes.forEach(function (item) {
				if(item.length > 0) {
					availableDataTypes.push(new Array(item, item));
				}
			});
		//}
		} else {
			var typeNames = new Array();
			COMSOFT.EXTENSIONS.dataTypes.forEach(function (item) {
				availableDataTypes.push(new Array(item.typeName, item.displayName));
				typeNames.push(item.typeName);
			});
			uniqueTypes = jQuery.unique(typeNames);
		}

		var Variable = Ext.data.Record.create([{
			name: 'name'
		}, {
			name: 'type'
		}, {
			name: 'caption'
		}, {
			name: 'defaultValue'
		}, {
			name: 'showInTaskList'
		}, {
			name: 'embedded'
		}, {
			name: 'selectionSource'
		}, {
			name: 'selectionProperty'
		}, {
			name: 'selectionCriteriaClass'
		}, {
			name: 'selectionCriteriaExpression'
		}, {
			name: 'selectionCriteriaParamsExpression'
		}, {
			name: '_variableInModel'
		}
		]);

		var variablesProxy = new Ext.data.MemoryProxy({
			root: []
		});

		variables = new Ext.data.Store({
			autoDestroy: true,
			reader: new Ext.data.JsonReader({
				root: "root"
			}, Variable),
			proxy: variablesProxy
			/*, sorters: [{
				property: 'name',
				direction:'ASC'
			}, {
            	property: 'to',
            	direction: 'ASC'
            }, {
            	property: 'tostr',
            	direction: 'ASC'
            } 
			]*/
		});
		variables.load();

		this.extensionVariablesDS = variables;

		var variablesFromExtensionMap = this.getVariablesFromExtensionMap();

		if (variablesFromExtensionMap.keys().length == 0) {
			processVarNames.forEach(function(varName){
				var newVar = new Variable({
					name: varName,
					type: dataTypeMap[varName],
					caption: '',
					defaultValue: '',
					showInTaskList: false,
					embedded: false,
					selectionSource: '',
					selectionProperty: '',
					selectionCriteriaClass: '',
					selectionCriteriaExpression: '',
					selectionCriteriaParamsExpression: ''
				});				
				variables.add(newVar);
			});			
		} else {
			variablesFromExtensionMap.keys().forEach(function(varName){
				var varFromExtension = variablesFromExtensionMap[varName];
				var varType = varFromExtension['type'] ? varFromExtension['type'] : dataTypeMap[varName];
				var newVar = new Variable({
					name: varName,
					type: varType,
					caption: varFromExtension['caption'] ? varFromExtension['caption'] : '',
					defaultValue: varFromExtension['defaultValue'] ? varFromExtension['defaultValue'] : '',
					showInTaskList: varFromExtension['showInTaskList'] ? varFromExtension['showInTaskList'] : false,
					embedded: varFromExtension['embedded'] ? varFromExtension['embedded'] : false,
					selectionSource: varFromExtension['selectionSource'] ? varFromExtension['selectionSource'] : '',
					selectionProperty: varFromExtension['selectionProperty'] ? varFromExtension['selectionProperty'] : '',
					selectionCriteriaClass: varFromExtension['selectionCriteriaClass'] ? varFromExtension['selectionCriteriaClass'] : '',
					selectionCriteriaExpression: varFromExtension['selectionCriteriaExpression'] ? varFromExtension['selectionCriteriaExpression'] : '',
					selectionCriteriaParamsExpression: varFromExtension['selectionCriteriaParamsExpression'] ? varFromExtension['selectionCriteriaParamsExpression'] : '',
					_variableInModel: varFromExtension
				});
				variables.add(newVar);
				
				if (uniqueTypes.indexOf(varType) < 0) {
					availableDataTypes.push(new Array(varType, varType));
				}
			});
		}

		/*        
        //keep sync between from and dataType
        dataassignments.on('update', function(store, record, operation){
            if (operation == "edit"){
                var newType = dataTypeMap[record.get("from")];
                if (!newType){
                    newType = "java.lang.String";
                }
                record.set("dataType", newType);
            }
        });
		 */

		var itemDeleter = new Extensive.grid.ItemDeleter();
		var gridId = Ext.id();
		var grid = new Ext.grid.EditorGridPanel({
			store: variables,
			id: gridId,
			stripeRows: true,
			cm: new Ext.grid.ColumnModel([new Ext.grid.RowNumberer()
			, {
				//id: 'name',
				header: 'Variable name',
				width: 150,
				dataIndex: 'name',
				editor: new Ext.form.ComboBoxEx({
					//id: 'varsCombo',
					valueField: 'value',
					displayField: 'name',
					//typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					//selectOnFocus:true,
					allowArbitraryText: true,
					store: new Ext.data.SimpleStore({
						fields: [
						         'value',
						         'name'
						         ],
						         data: varData
					})
				})
			}
			, {
				//id: 'type',
				header: 'Data Type',
				width: 150,
				dataIndex: 'type',
				editor: new Ext.form.ComboBoxEx({
					//id: 'typeCombo',
					valueField: 'value',
					displayField: 'name',
					//typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					//selectOnFocus:true,
					allowArbitraryText: true,
					store: new Ext.data.SimpleStore({
						fields: [   
						         'value',
						         'name'
						         ],
						data: availableDataTypes
					})
				})
			}
			, {
				//id: 'caption',
				header: 'Caption',
				width: 150,
				dataIndex: 'caption',
				editor: new Ext.form.TextField({ allowBlank: true }),
				renderer: this.renderer.bind(this)
			}
			, {
				//id: 'showInTaskList',
				header: 'Show in task list',
				width: 60,
				dataIndex: 'showInTaskList',
				editor: new Ext.form.Checkbox({ allowBlank: true })
			}
			, {
				//id: 'embedded',
				header: 'Embedded',
				width: 60,
				dataIndex: 'embedded',
				editor: new Ext.form.Checkbox({ allowBlank: true })
			}
			, {
				//id: 'selectionSource',
				header: 'Selection source',
				width: 60,
				dataIndex: 'selectionSource',
				editor: new Ext.form.TextField({ allowBlank: true }),
				renderer: this.renderer.bind(this)
			}
			, {
				//id: 'selectionCriteriaExpression',
				header: 'Selection criteria expression',
				width: 60,
				dataIndex: 'selectionCriteriaExpression',
				editor: new Ext.form.TextField({ allowBlank: true }),
				renderer: this.renderer.bind(this)
			}
			, itemDeleter]),

			selModel: itemDeleter,
    		//autoHeight: true,
    		tbar: [{
    			text: 'Add Variable',
    			handler : function(){
    				variables.add(new Variable({
    					name: '',
    					type: '',
    					caption: '',
    					showInTaskList: false,
    					embedded: false,
    					selectionSource: '',
    					selectionCriteriaExpression: ''
    				}));
    				grid.fireEvent('cellclick', grid, variables.getCount()-1, 1, null);
    			}
    		}],
    		clicksToEdit: 1
		});

		return grid;
	},

	createAttributesGrid: function(subjectType, subjectId, loadedAttributes, options) {
		var taskNodeName = options ? options.taskNodeName : undefined;
		var outcomes = options ? options.outcomes : undefined;

		var isProcess = (subjectType === 'Process');
		var isUserTask = (subjectType === 'HumanTask');
		var isSequenceFlow = (subjectType === 'SequenceFlow');

		var keysData = new Array();
		var subjectIds = new Array();
		if (isProcess) {			
			keysData.push(new Array('manualStartEnabled', 'Ручной запуск (manualStartEnabled)'));
			keysData.push(new Array('controllerFor', 'controllerFor'));
			keysData.push(new Array('taskListField_ЗАГОЛОВОК', 'Вычисляемое поле в списке (taskListField_ЗАГОЛОВОК)'));			
			keysData.push(new Array('taskListFieldType_ЗАГОЛОВОК', 'Тип вычисляемого поля в списке (taskListFieldType_ЗАГОЛОВОК)'));			
		} else if (isUserTask) {
			keysData.push(new Array('JBPM5_START_STOP_FUNCTIONS', 'Вкл. кнопки Приступить/Отложить (JBPM5_START_STOP_FUNCTIONS)'));
			keysData.push(new Array('NOTIFICATIONS.ENABLED', 'Вкл. рассылку уведомлений (NOTIFICATIONS.ENABLED)'));
			keysData.push(new Array('NOTIFICATIONS.SUBJECT', 'Тема уведомления (NOTIFICATIONS.SUBJECT)'));
			keysData.push(new Array('NOTIFICATIONS.TEMPLATE', 'Шаблон уведомления (NOTIFICATIONS.TEMPLATE)'));
			keysData.push(new Array('taskRevertScript', 'Скрипт возврата (taskRevertScript)'));
			keysData.push(new Array('taskOperation_ЗАГОЛОВОК', 'Операция (taskOperation_ЗАГОЛОВОК)'));
		} else if (isSequenceFlow) {			
			keysData.push(new Array('NOVALIDATE', 'Не выполнять валидацию (NOVALIDATE)'));
			keysData.push(new Array('NOVALIDATE_NOSAVE', 'Не выполнять валидацию и сохранение данных (NOVALIDATE_NOSAVE)'));
			keysData.push(new Array('confirmation', 'Текст подтверждения (confirmation)'));
			keysData.push(new Array('confirmationIf', 'Условие подтверждения (confirmationIf)'));
			keysData.push(new Array('visible', 'Доступность/видимость (visible)'));
			outcomes.forEach(function (outcome) {
				subjectIds.push(new Array(taskNodeName + ';' + outcome, outcome));
			});
		}

		var valuesData = new Array();
		valuesData.push(new Array('1', 'Включить (1)'));
		valuesData.push(new Array('0', 'Выключить (0)'));

		var Attribute = Ext.data.Record.create([{
			name: 'key'
		}, {
			name: 'value'
		}, {
			name: 'subjectType'				
		}, {
			name: 'subjectId'				
		}, {
			name: '_attributeInModel'
		}
		]);

		var attributesProxy = new Ext.data.MemoryProxy({
			root: []
		});

		var attributes = new Ext.data.Store({
			autoDestroy: true,
			reader: new Ext.data.JsonReader({
				root: "root"
			}, Attribute),
			proxy: attributesProxy
			/*,sorters: [{
				property: 'name',
				direction:'ASC'
			}, {
            	property: 'to',
            	direction: 'ASC'
            }, {
            	property: 'tostr',
            	direction: 'ASC'
            } 
			]*/
		});
		attributes.load();

		if (!isSequenceFlow) {
			loadedAttributes = new Array();
			this.loadedAttributes = loadedAttributes;			
			this.extensionAttributesDS = attributes;
			this.getAttributesFromModel(subjectType, subjectId).forEach(function (attribute) {
				var newAttr = new Attribute({
					key: attribute.key,
					value: attribute.value,
					subjectType: attribute.subjectType,
					subjectId: attribute.subjectId,
					_attributeInModel: attribute
				});
				attributes.add(newAttr);
				loadedAttributes.push(attribute);
			});
		} else {
			this.extensionSeqFlowAttributesDS = attributes;
			var deletedSequenceFlows = COMSOFT.EXTENSIONS.deletedSequenceFlows;
			COMSOFT.EXTENSIONS.model['attributes'].forEach(function (attribute) {
				if (deletedSequenceFlows.indexOf(attribute.subjectId) < 0 && attribute.subjectType === subjectType && attribute.subjectId.indexOf(taskNodeName + ';') == 0) {
					var newAttr = new Attribute({
						key: attribute.key,
						value: attribute.value,
						subjectType: attribute.subjectType,
						subjectId: attribute.subjectId,
						_attributeInModel: attribute
					});
					attributes.add(newAttr);
					loadedAttributes.push(attribute);
				}
			});
		}

		var itemDeleter = new Extensive.grid.ItemDeleter();
		
		var columns = new Array();
		columns.push(new Ext.grid.RowNumberer());
		if (subjectIds && subjectIds.length > 0)
			columns.push(
					{
						//id: 'subjectId',
						header: 'Subject Id',
						width: 300,
						dataIndex: 'subjectId',
						editor: new Ext.form.ComboBox({
							//id: 'subjectIdsCombo',
							valueField: 'value',
							displayField: 'name',
							//typeAhead: true,
							mode: 'local',
							triggerAction: 'all',
							//selectOnFocus:true,
							store: new Ext.data.SimpleStore({
								fields: ['value', 'name'],
								data: subjectIds
							})
						})
					}
			);
		columns.push(
				{
					//id: 'key',
					header: 'Key',
					width: 300,
					dataIndex: 'key',
					editor: new Ext.form.ComboBoxEx({
						//id: 'keysCombo',
						valueField: 'value',
						displayField: 'name',
						//typeAhead: true,
						mode: 'local',
						triggerAction: 'all',
						//selectOnFocus:true,
						allowArbitraryText: true,
						store: new Ext.data.SimpleStore({
							fields: ['value', 'name'],
							data: keysData
						})
					})
				}
		);
		columns.push(
				{
					//id: 'value',
					header: 'Value',
					width: 250,
					dataIndex: 'value',
					editor: new Ext.form.ComboBoxEx({
						//id: 'valuesCombo',
						valueField: 'value',
						displayField: 'name',
						//typeAhead: true,
						mode: 'local',
						triggerAction: 'all',
						//selectOnFocus:true,
						allowArbitraryText: true,
						store: new Ext.data.SimpleStore({
							fields: ['value', 'name'],
							data: valuesData
						})
					}),
					renderer: this.renderer.bind(this)
				}
		);
		columns.push(itemDeleter);
		
		var gridId = Ext.id();
		var grid = new Ext.grid.EditorGridPanel({
			store: attributes,
			id: gridId,
			stripeRows: true,
			cm: new Ext.grid.ColumnModel(columns),
			selModel: itemDeleter,
    		//autoHeight: true,
    		tbar: [{
    			text: 'Add Attribute',
    			handler : function(){
    				attributes.add(new Attribute({
    					key: '',
    					value: '',
    					subjectType: subjectType,
    					subjectId: (!isSequenceFlow ? subjectId : '')
    				}));
    				grid.fireEvent('cellclick', grid, attributes.getCount()-1, 1, null);
    			}
    		}],
    		clicksToEdit: 1
		});

		return grid;
	},

	createTaskVariablesGrid: function(task) {
		var TaskVariable = Ext.data.Record.create([{
			name: 'task'
		}, {
			name: 'var'
		}, {
			name: 'readonly'				
		}, {
			name: 'required'				
		}, {
			name: 'hidden'				
		}, {
			name: 'varPosition'
		}, {
			name: 'entityHomeMode'				
		}, {
			name: '_varInModel'
		}
		]);

		var taskVariablesProxy = new Ext.data.MemoryProxy({
			root: []
		});

		var taskVariables = new Ext.data.Store({
			autoDestroy: true,
			reader: new Ext.data.JsonReader({
				root: "root"
			}, TaskVariable),
			proxy: taskVariablesProxy
			/*,sorters: [{
				property: 'name',
				direction:'ASC'
			}, {
            	property: 'to',
            	direction: 'ASC'
            }, {
            	property: 'tostr',
            	direction: 'ASC'
            } 
			]*/
		});
		taskVariables.load();

		this.extensionTaskVariablesDS = taskVariables;

		this.getTaskVariablesFromExtensionMap(task).values().forEach(function (taskVar) {
			if (taskVar.task && taskVar['var']) {				
				var newVar = new TaskVariable({
					task: taskVar.task,
					'var': taskVar['var'],
					readonly: taskVar.readonly,
					required: taskVar.required,
					hidden: taskVar.hidden,
					varPosition: (taskVar.varPosition ? taskVar.varPosition : ''),
					entityHomeMode: (taskVar.entityHomeMode ? taskVar.entityHomeMode : ''),
					_varInModel: taskVar
				});
				taskVariables.add(newVar);
			}
		});
		
		var varsData = new Array();
		this.getVariablesFromExtensionMap().values().forEach(function (variable) {
			varsData.push(new Array(variable.name, variable.caption + ' (' + variable.name + ')'));
		});

		var itemDeleter = new Extensive.grid.ItemDeleter();
		var gridId = Ext.id();
		var grid = new Ext.grid.EditorGridPanel({
			store: taskVariables,
			id: gridId,
			stripeRows: true,
			cm: new Ext.grid.ColumnModel([new Ext.grid.RowNumberer()
			, {
				//id: 'var',
				header: 'Variable',
				width: 150,
				dataIndex: 'var',
				editor: new Ext.form.ComboBoxEx({
					//id: 'varsCombo',
					valueField: 'value',
					displayField: 'name',
					//typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					//selectOnFocus:true,
					allowArbitraryText: true,
					store: new Ext.data.SimpleStore({
						fields: ['value', 'name'],
						data: varsData
					})
				})
			}
			, {
				//id: 'readonly',
				header: 'Readonly',
				width: 60,
				dataIndex: 'readonly',
				editor: new Ext.form.Checkbox({ allowBlank: true })
			}
			, {
				//id: 'required',
				header: 'Required',
				width: 60,
				dataIndex: 'required',
				editor: new Ext.form.Checkbox({ allowBlank: true })
			}
			, {
				//id: 'hidden',
				header: 'Hidden',
				width: 60,
				dataIndex: 'hidden',
				editor: new Ext.form.Checkbox({ allowBlank: true })
			}
			, {
				//id: 'varPosition',
				header: 'Position',
				width: 60,
				dataIndex: 'varPosition',
				editor: new Ext.form.NumberField({ allowBlank: true, allowDecimals: false })
			}
			, {
				//id: 'entityHomeMode',
				header: 'Entity Home Mode',
				width: 200,
				dataIndex: 'entityHomeMode',
				editor: new Ext.form.TextField({ allowBlank: true })
			}
			, itemDeleter]),
			selModel: itemDeleter,
    		//autoHeight: true,
    		tbar: [{
    			text: 'Add Variable',
    			handler : function(){
    				taskVariables.add(new TaskVariable({
    					task: task,
    					'var': '',
    					readonly: undefined,
    					required: undefined,
    					entityHomeMode: ''
    				}));
    				grid.fireEvent('cellclick', grid, taskVariables.getCount()-1, 1, null);
    			}
    		}],
    		clicksToEdit: 1
		});

		return grid;
	},

	createTaskValidationsGrid: function(task) {
		var TaskValidation = Ext.data.Record.create([{
			name: 'task'
		}, {
			name: 'condition'
		}, {
			name: 'errorMessage'				
		}, {
			name: '_validationInModel'
		}
		]);

		var taskValidationsProxy = new Ext.data.MemoryProxy({
			root: []
		});

		var taskValidations = new Ext.data.Store({
			autoDestroy: true,
			reader: new Ext.data.JsonReader({
				root: "root"
			}, TaskValidation),
			proxy: taskValidationsProxy
			/*,sorters: [{
				property: 'name',
				direction:'ASC'
			}, {
            	property: 'to',
            	direction: 'ASC'
            }, {
            	property: 'tostr',
            	direction: 'ASC'
            } 
			]*/
		});
		taskValidations.load();

		this.extensionTaskValidationsDS = taskValidations;

		this.getTaskValidationsFromExtensionMap(task).values().forEach(function (taskValidation) {
			if (taskValidation.task && taskValidation.condition) {				
				var newValidation = new TaskValidation({
					task: taskValidation.task,
					condition: taskValidation.condition,
					errorMessage: (taskValidation.errorMessage ? taskValidation.errorMessage : ''),
					_validationInModel: taskValidation
				});
				taskValidations.add(newValidation);
			}
		});

		var itemDeleter = new Extensive.grid.ItemDeleter();
		var gridId = Ext.id();
		var grid = new Ext.grid.EditorGridPanel({
			store: taskValidations,
			id: gridId,
			stripeRows: true,
			cm: new Ext.grid.ColumnModel([new Ext.grid.RowNumberer()
			, {
				//id: 'var',
				header: 'Condition',
				width: 300,
				dataIndex: 'condition',
				editor: new Ext.form.TextField({ allowBlank: false }),
				renderer: this.renderer.bind(this)
			}
			, {
				//id: 'entityHomeMode',
				header: 'Error message',
				width: 300,
				dataIndex: 'errorMessage',
				editor: new Ext.form.TextField({ allowBlank: false }),
				renderer: this.renderer.bind(this)
			}
			, itemDeleter]),
			selModel: itemDeleter,
    		//autoHeight: true,
    		tbar: [{
    			text: 'Add Validation',
    			handler : function(){
    				taskValidations.add(new TaskValidation({
    					task: task,
    					condition: '',
    					errorMessage: ''
    				}));
    				grid.fireEvent('cellclick', grid, taskValidations.getCount()-1, 1, null);
    			}
    		}],
    		clicksToEdit: 1
		});

		return grid;
	},

	renderer: function(value, p, record) {			
		if(String(value).search("<a href='") < 0) {
			// Shows the Value in the Grid in each Line
			value = String(value).gsub("<", "&lt;");
			value = String(value).gsub(">", "&gt;");
			value = String(value).gsub("%", "&#37;");
			value = String(value).gsub("&", "&amp;");
		}

		return value;
	},

	saveVariables: function() {
		var varsInModel = COMSOFT.EXTENSIONS.model['variables'];
		if (!varsInModel) {
			varsInModel = new Array();
			COMSOFT.EXTENSIONS.model['variables'] = varsInModel;
		}
		var variablesFromExtensionMap = this.getVariablesFromExtensionMap();
		var ds = this.extensionVariablesDS;
		//ds.commitChanges();
		var fieldsToSave = [
			'type', 'caption', 'defaultValue', 'showInTaskList', 'embedded', 'selectionSource', 'selectionProperty',
			'selectionCriteriaClass', 'selectionCriteriaExpression', 'selectionCriteriaParamsExpression'
		];
		var actualVarNames = new Array();
		var renamedVars = new Hash();
		var deletedVars = new Array();
		for (var i = 0; i < ds.getCount(); i++) {
			var data = ds.getAt(i);
			var varName = data.get('name');
			actualVarNames.push(varName);
			var varInModel = data.get('_variableInModel');
			if (!varInModel) {
				varInModel = variablesFromExtensionMap[varName];
				if (!varInModel) {
					varInModel = new Hash();
					varsInModel.push(varInModel);
				}
			} else if (varInModel['name'] && varInModel['name'].length > 0 && varInModel['name'] !== varName) {
				renamedVars[varInModel['name']] = varName;
			}
			varInModel['name'] = varName;
			for (var j = 0; j < fieldsToSave.length; j++) {
				var fieldName = fieldsToSave[j];
				var fieldValue = data.get(fieldName);
				if (fieldValue && (typeof fieldValue !== 'string' || fieldValue.length > 0)) {
					varInModel[fieldName] = fieldValue;
				} else if (varInModel[fieldName]) {
					delete varInModel[fieldName];
				}
			}
		}
		varsInModel.forEach(function (item) {
			if (actualVarNames.indexOf(item['name']) < 0) {
				varsInModel.splice(varsInModel.indexOf(item), 1);
				//varsToRemove.push(item['name']);
				deletedVars.push(item['name']);
			}
		});
		console.info('renamedVars = ' + renamedVars);
		console.info('deletedVars = ' + deletedVars);
		if (renamedVars.keys().length > 0 || deletedVars.length > 0) {
			var taskVarsInModel = COMSOFT.EXTENSIONS.model['taskVariables'];
			if (taskVarsInModel) {
				var i = 0;
				while (i < taskVarsInModel.length) {
					var taskVar = taskVarsInModel[i];
					if (renamedVars[taskVar['var']]) taskVar['var'] = renamedVars[taskVar['var']];
					if (deletedVars.indexOf(taskVar['var']) >= 0) {
						taskVarsInModel.splice(i, 1);
					} else {
						i++;
					}
				}
			}
		}
	},

	saveTaskVariables: function(task) {
		var varsInModel = COMSOFT.EXTENSIONS.model['taskVariables'];
		if (!varsInModel) {
			varsInModel = new Array();
			COMSOFT.EXTENSIONS.model['taskVariables'] = varsInModel;
		}
		var variablesFromExtensionMap = this.getTaskVariablesFromExtensionMap(task);
		var ds = this.extensionTaskVariablesDS;
		//ds.commitChanges();
		var fieldsToSave = ['readonly', 'required', 'hidden', 'varPosition', 'entityHomeMode'];
		var actualVarNames = new Array();
		for (var i = 0; i < ds.getCount(); i++) {
			var data = ds.getAt(i);
			var varName = data.get('var');
			actualVarNames.push(varName);
			var varInModel = data.get('_variableInModel');
			if (!varInModel) {
				varInModel = variablesFromExtensionMap[varName];
				if (!varInModel) {
					varInModel = new Hash();
					varsInModel.push(varInModel);
				}
			}
			varInModel['var'] = varName;
			varInModel['task'] = task;
			for (var j = 0; j < fieldsToSave.length; j++) {
				var fieldName = fieldsToSave[j];
				var fieldValue = data.get(fieldName);
				if (fieldValue && (typeof fieldValue !== 'string' || fieldValue.length > 0)) {
					varInModel[fieldName] = fieldValue;
				} else if (varInModel[fieldName]) {
					delete varInModel[fieldName];
				}
			}
		}

		this.getTaskVariablesFromExtensionMap(task).values().forEach(function (item) {
			if (actualVarNames.indexOf(item['var']) < 0) {
				varsInModel.splice(varsInModel.indexOf(item), 1);
			}
		});
	},

	saveTaskValidations: function(task) {
		var validationsInModel = COMSOFT.EXTENSIONS.model['taskValidations'];
		if (!validationsInModel) {
			varsInModel = new Array();
			COMSOFT.EXTENSIONS.model['taskValidations'] = validationsInModel;
		}
		var validationsFromExtensionMap = this.getTaskValidationsFromExtensionMap(task);
		var ds = this.extensionTaskValidationsDS;
		//ds.commitChanges();
		var actualConditions = new Array();
		for (var i = 0; i < ds.getCount(); i++) {
			var data = ds.getAt(i);
			var condition = data.get('condition');
			var errorMessage = data.get('errorMessage');
			actualConditions.push(condition);
			var validationInModel = data.get('_validationInModel');
			if (!validationInModel) {
				validationInModel = validationsFromExtensionMap[condition];
				if (!validationInModel) {
					validationInModel = new Hash();
					validationsInModel.push(validationInModel);
				}
			}
			validationInModel.task = task;
			validationInModel.condition = condition;
			validationInModel.errorMessage = errorMessage;
		}

		this.getTaskValidationsFromExtensionMap(task).values().forEach(function (item) {
			if (actualConditions.indexOf(item.condition) < 0) {
				validationsInModel.splice(validationsInModel.indexOf(item), 1);
			}
		});
	},

	saveAttributes: function() {
		var attributesInModel = COMSOFT.EXTENSIONS.model['attributes'];
		if (!attributesInModel) {
			attributesInModel = new Array();
			COMSOFT.EXTENSIONS.model['attributes'] = attributesInModel;
		}
		var actualAttributes = new Array();
		var dataStores = new Array(this.extensionAttributesDS, this.extensionSeqFlowAttributesDS);
		for (var dsidx = 0; dsidx < dataStores.length; dsidx++) {			
			ds = dataStores[dsidx];
			if (!ds) continue;
			fieldsToSave = [/*'key', */'value'/*, 'subjectType', 'subjectId'*/];
			for (var i = 0; i < ds.getCount(); i++) {
				var data = ds.getAt(i);
				var subjectType = data.get('subjectType');
				var subjectId = data.get('subjectId');
				var attrKey = data.get('key');
				var attrValue = data.get('value');
				if (attrValue && attrValue.length > 0) {
					actualAttributes.push(subjectType + (subjectId ? '/' + subjectId : '') + ':' + attrKey + '=' + attrValue);
					var setupValues = false;
					var attrInModel = data.get('_attributeInModel');
					if (!attrInModel) {
						attrInModel = this.getAttributeFromModel(subjectType, subjectId, attrKey, attrValue);
						if (!attrInModel) {
							attrInModel = new Hash();
							attributesInModel.push(attrInModel);
							setupValues = true;
						}
					} else {
						setupValues = true;
					}
					if (setupValues) {
						attrInModel['subjectType'] = subjectType;
						attrInModel['subjectId'] = subjectId;
						attrInModel['key'] = attrKey;
						attrInModel['value'] = attrValue;
					}
				}
			}
		}
		this.loadedAttributes.forEach(function (item) {
			var subjectType = item['subjectType'];
			var subjectId = item['subjectId'];
			var attrKey = item['key'];
			var attrValue = item['value'];
			var needRemove = false;
			if (!attrValue || attrValue.length == 0) {
				needRemove = true;
			} else {
				console.info('check ' + subjectType);
				if (actualAttributes.indexOf(subjectType + (subjectId ? '/' + subjectId : '') + ':' + attrKey + '=' + attrValue) < 0) {
					needRemove = true;
				}
			}
			if (needRemove) {
				console.info('remove ' + item.value);
				attributesInModel.splice(attributesInModel.indexOf(item), 1);
			}
		});		
	},

	getVariablesFromExtensionMap: function() {
		var result = new Hash();	
		if (COMSOFT.EXTENSIONS.model['variables']) {
			COMSOFT.EXTENSIONS.model['variables'].forEach(function(varFromExtension) {
				result[varFromExtension['name']] = varFromExtension;
				//alert("add " + varFromExtension['name'] + " : " + varFromExtension);
			});
		}
		return result;
	},

	getTaskVariablesFromExtensionMap: function(task) {
		var result = new Hash();	
		if (COMSOFT.EXTENSIONS.model['taskVariables']) {
			COMSOFT.EXTENSIONS.model['taskVariables'].forEach(function(varFromExtension) {
				if (varFromExtension['task'] === task) {					
					result[varFromExtension['var']] = varFromExtension;
				}
			});
		}
		return result;
	},

	getTaskValidationsFromExtensionMap: function(task) {
		var result = new Hash();	
		if (COMSOFT.EXTENSIONS.model['taskValidations']) {
			COMSOFT.EXTENSIONS.model['taskValidations'].forEach(function(validationFromExtension) {
				if (validationFromExtension['task'] === task) {					
					result[validationFromExtension['condition']] = validationFromExtension;
				}
			});
		}
		return result;
	},

	getAttributesFromModel: function(subjectType, subjectId) {
		var result = new Array();
		if (COMSOFT.EXTENSIONS.model['attributes']) {
			COMSOFT.EXTENSIONS.model['attributes'].forEach(function(attribute) {
				if (attribute.subjectType === subjectType && attribute.subjectId === subjectId) {
					result.push(attribute);
				}
			});
		}
		return result;
	},

	getAttributeFromModel: function(subjectType, subjectId, attrKey, attrValue) {
		if (COMSOFT.EXTENSIONS.model['attributes']) {
			COMSOFT.EXTENSIONS.model['attributes'].forEach(function(attr) {
				if (attr.subjectType === subjectType && attr.subjectId === subjectId && attr.key === attrKey && attr.value === attrValue) {
					return attr;
				}
			});
		}
		return undefined;
	},

	findUserTaskShape: function (shapes, taskName) {
	    if (!shapes || !taskName) return undefined;
	    for (var i = 0; i < shapes.length; i++) {
	        var shape = shapes[i];
	        if (shape.stencil.id === 'Task' && shape.properties.tasktype === 'User' && shape.properties.name === taskName) return shape;
	        if (shape.childShapes && shape.childShapes.length > 0) {
	            var innerCallResult = this.findUserTaskShape(shape.childShapes, taskName);
	            if (innerCallResult) return innerCallResult;
	        }
	    };
	    return undefined;
	},
	findShape: function (shapes, resourceId, stencilId) {
	    if (!shapes || !resourceId) return undefined;
	    for (var i = 0; i < shapes.length; i++) {
	        var shape = shapes[i];
	        if (shape.resourceId === resourceId && (!stencilId || shape.stencil.id === stencilId)) return shape;
	        if (shape.childShapes && shape.childShapes.length > 0) {
	            var innerCallResult = this.findShape(shape.childShapes, resourceId, stencilId);
	            if (innerCallResult) return innerCallResult;
	        }
	    };
	    return undefined;
	},
	getUserTaskOutcomes: function (taskName) {
		var childShapes = JSON.parse(ORYX.EDITOR.getSerializedJSON()).childShapes;
		var userTaskShape = this.findUserTaskShape(childShapes, taskName);
		var outcomes = new Array();
		if (userTaskShape && userTaskShape.outgoing && userTaskShape.outgoing.length > 0) {
		    var seqFlowShape = this.findShape(childShapes, userTaskShape.outgoing[0].resourceId, 'SequenceFlow');
		    if (seqFlowShape) {
		        nextShape = this.findShape(childShapes, seqFlowShape.outgoing[0].resourceId);
		        if (nextShape.properties.hasOwnProperty('gatewaytype') && nextShape.outgoing && nextShape.outgoing.length > 0) {
		        	var gwType = nextShape.properties.gatewaytype;
		        	if (gwType === 'XOR' || gwType === 'OR') { 
			            for (var i = 0; i < nextShape.outgoing.length; i++) {
			                var ocSeqFlowShape = this.findShape(childShapes, nextShape.outgoing[i].resourceId, 'SequenceFlow');
			                if (ocSeqFlowShape && ocSeqFlowShape.properties.conditionexpression && ocSeqFlowShape.properties.name) {
			                    var expr = ocSeqFlowShape.properties.conditionexpression;
			                    if (this.checkOutcomeConditionExpression(expr)) {
			                        outcomes.push(ocSeqFlowShape.properties.name);
			                    }
			                }
			            }
			            if (outcomes.length > 0) return outcomes;
		        	}
		        }
		        if (seqFlowShape.properties.name && seqFlowShape.properties.name.length > 0) {
		        	outcomes.push(seqFlowShape.properties.name);
		        	return outcomes;
		        }
		    }
		}
		outcomes.push('Завершить'); // org.comsoft.jbpm5.task.Jbpm5TaskUtils.DEFAULT_OUTCOME_NAME
		return outcomes;
	},
	checkOutcomeConditionExpression: function (expr) {
        var pattern1 = /\s{0,}return\s+([\w\dа-я]+)\s{0,}==\s{0,}\"(.+)\".{0,};.{0,}/gi;
        var pattern2 = /\s{0,}return\s+([\w\dа-я]+)\s{0,}\.\s{0,}equals\s{0,}\(\s{0,}\"(.+)\"\s{0,}\).{0,};.{0,}/gi;
        var pattern1Matches = pattern1.test(expr);
        var pattern2Matches = pattern2.test(expr);
        return (pattern1Matches || pattern2Matches);
	},
	getSubjectIdForSequenceFlow: function (sequenceFlowShape) {
		var seqFlowShape = sequenceFlowShape;
		if (!seqFlowShape || !seqFlowShape.incoming || seqFlowShape.incoming.length == 0) return undefined;
		if (!seqFlowShape.properties.hasOwnProperty('oryx-name') && seqFlowShape.properties['oryx-name'].length == 0) return undefined;
		if (!seqFlowShape.properties.hasOwnProperty('oryx-conditionexpression')) return undefined;
		var expr = seqFlowShape.properties['oryx-conditionexpression'];
        if (this.checkOutcomeConditionExpression(expr)) {        	
        	var prevShape = seqFlowShape.incoming[0];
        	if (prevShape.properties.hasOwnProperty('oryx-gatewaytype') && prevShape.incoming && prevShape.incoming.length == 1) {
        		var gwIncomingSeqFlow = prevShape.incoming[0];
        		if (!gwIncomingSeqFlow.incoming || gwIncomingSeqFlow.incoming.length == 0) return undefined;
        		var prevPrevShape = gwIncomingSeqFlow.incoming[0];
    			var stencilId = prevPrevShape._stencil.id();
    			var stencilType = stencilId.substring(stencilId.indexOf("#") + 1);
    			if (stencilType === 'Task' && prevPrevShape.properties.hasOwnProperty('oryx-name') && prevPrevShape.properties['oryx-name'].length > 0) {
    				return prevPrevShape.properties['oryx-name'] + ';' + seqFlowShape.properties['oryx-name'];
    			}
        	}
        }
        return undefined;
	},

	propertyChanged: function( option, node){
		if (!node instanceof ORYX.Core.Node) return;
		var oldValue = option.oldValue;
		var newValue = option.value;
		var model = COMSOFT.EXTENSIONS.model;
		if (!model || !oldValue || !newValue || oldValue.length == 0 || newValue.length == 0) return;
		var propname = option.name;
		try {
			if( propname === "oryx-name" ) {
				var stencilId = node.getStencil().id();
				var stencilType = stencilId.substring(stencilId.indexOf("#") + 1);
				if (stencilType === 'Task') {
					for (modelKey in model) {
						if (model.hasOwnProperty(modelKey)) {
							var modelValue = model[modelKey];
							if (modelValue.hasOwnProperty('length') && modelValue.push) {
								for (var i = 0; i < modelValue.length; i++) {
									var el = modelValue[i];
									if (el.hasOwnProperty('task') && el.task == oldValue) {
										console.info('change task name [' + el.task + '] to [' + newValue + ']');
										el.task = newValue;
									} else if (el.hasOwnProperty('subjectId') && el.hasOwnProperty('subjectType')) {
										if (el.subjectType === 'HumanTask' && el.subjectId == oldValue) {
											console.info('change task name in task attribute [' + el.subjectId + '] to [' + newValue + ']');
											el.subjectId = newValue;
										} else if (el.subjectType === 'SequenceFlow' && el.subjectId.indexOf(oldValue + ';') == 0) {
											var newSubjectId = newValue + ';' + el.subjectId.substring((oldValue + ';').length);
											console.info('change task name in SequenceFlow attribute [' + el.subjectId + '] to [' + newSubjectId + ']');
											el.subjectId = newSubjectId;
										}
									}
								}
							}
						}
					}
				} else if (stencilType === 'SequenceFlow' && model.attributes) {
					var attrs = model.attributes;
					for (var i = 0; i < attrs.length; i++) {
						var el = attrs[i];
						if (el.subjectType === 'SequenceFlow' && el.subjectId && el.subjectId.indexOf(';' + oldValue) > 0) {
							var newSubjectId = el.subjectId.substring(0, el.subjectId.indexOf(';' + oldValue)) + ';' + newValue;
							console.info('change SequenceFlow name in SequenceFlow attribute [' + el.subjectId + '] to [' + newSubjectId + ']');
							el.subjectId = newSubjectId;
						}
					}
				}
			} else if( propname === "oryx-id" ) {
				console.info('change processKey [' + model.processKey + '] to [' + newValue + ']');
				model.processKey = newValue;
			}
		} catch (e) {
			Ext.Msg.minWidth = 360;
			Ext.Msg.alert('Error after rename:\n' + e);
		}
	},

	shapeAdded: function(option) {
		var shape = option.shape;
		if (shape.hasOwnProperty('_stencil') && shape.hasOwnProperty('properties')) {
			var stencilId = shape._stencil.id();
			var stencilType = stencilId.substring(stencilId.indexOf("#") + 1);
			if (stencilType === 'Task') {
				var deletedTasks = COMSOFT.EXTENSIONS.deletedTasks;
				var index = deletedTasks.indexOf(shape.properties['oryx-name']);
				if (index >= 0) deletedTasks.splice(index, 1);
				console.info('deletedTaks: ' + deletedTasks);
			} else if (stencilType === 'SequenceFlow') {
				var deletedSequenceFlows = COMSOFT.EXTENSIONS.deletedSequenceFlows;
				var value = this.getSubjectIdForSequenceFlow(shape);
				var index = deletedSequenceFlows.indexOf(value);
				if (index >= 0) deletedSequenceFlows.splice(index, 1);
				console.info('deletedSequenceFlows: ' + deletedSequenceFlows);
			}
		}
	},
	beforeShapeDeleted: function(option) {
		var shape = option.value;
		if (shape.hasOwnProperty('_stencil') && shape.hasOwnProperty('properties')) {
			var stencilId = shape._stencil.id();
			var stencilType = stencilId.substring(stencilId.indexOf("#") + 1);
			if (stencilType === 'Task') {
				var deletedTasks = COMSOFT.EXTENSIONS.deletedTasks;
				if (deletedTasks.indexOf(shape.properties['oryx-name']) < 0)
					deletedTasks.push(shape.properties['oryx-name']);
				console.info('deletedTaks: ' + deletedTasks);
			} else if (stencilType === 'SequenceFlow') {
				var deletedSequenceFlows = COMSOFT.EXTENSIONS.deletedSequenceFlows;
				var value = this.getSubjectIdForSequenceFlow(shape);
				if (deletedSequenceFlows.indexOf(value) < 0)
					deletedSequenceFlows.push(value);				
				console.info('deletedSequenceFlows: ' + deletedSequenceFlows);
			}
		}
	}

});